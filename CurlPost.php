<?php
/*
/ 
	Usage:
	$docStylesheet = "/Users/DAT/Documents/My Web Sites/mymsf-portal/MSF-DonationConf-D.odt";
	$inputXml = "<?xml version=\"1.0\" encoding=\"utf8\"?>";
	$inputXml  = $inputXml .  "<foo><bar><date>".date("d.m.y")."</date><Organisation>NoPostData</Organisation><FirstLast>OrgDenisAbt</FirstLast><Address>Elisabethenstrasse 26</Address><Zip>8004</Zip><City>Zurich</City><Country>Schweiz</Country><Year>2016</Year><Amount>1255</Amount></bar></foo>";

/
*/

require __DIR__ . "/vendor/autoload.php";
require_once(__DIR__ . '/classes/Helpers.php');
date_default_timezone_set('Europe/Zurich');

if (strpos($_SERVER["HTTP_HOST"], 'getunik.') !== false) {
	$data = spyc_load_file('config/config_prod.yaml');
} else {
	$data = spyc_load_file('config/config_dev.yaml');
}

$serviceUrl = $data["ServiceUrl"];

// We require templateValues and userfile / 
$filename  = dirname(__FILE__) . DIRECTORY_SEPARATOR . "_templates" . DIRECTORY_SEPARATOR . "MSF" . DIRECTORY_SEPARATOR . "MSF-DonationConf-I.odt";
$inputXml = "<?xml version=\"1.0\" encoding=\"utf8\"?>";
$inputXml  = $inputXml .  "<foo><bar><date>".date("d.m.y")."</date><Organisation>getunik AG</Organisation><FirstLast>Denis Abt</FirstLast><Address>Hardturmstrasse 101</Address><Zip>8005</Zip><City>Z�rich</City><Country>CH</Country><Year>2015</Year><Amount>233</Amount></bar></foo>";
$inputXml = utf8_encode($inputXml);

$result = generatePdf($serviceUrl, $filename, $inputXml);

echo "<BR>Calling service at $serviceUrl" . "<BR>" . PHP_EOL;
echo "Submit succeeded. <br>Response: <A HREF='" . $result . "'>" . $result . "</A>" . "<BR>" . PHP_EOL;


function generatePdf($serviceUrl, $filename, $fieldValueXml) {
	
	$ch = curl_init($serviceUrl);
	$logfile = dirname(__FILE__) . DIRECTORY_SEPARATOR . "errorlog.txt";
	$fp = fopen($logfile, 'a');
	echo "Using log file $logfile";
	fwrite($fp, "generatePdf triggered at ". date("Y-m-d H:i:s") . PHP_EOL) ;
	
	$filebasename = basename($filename);

	$args['userfile'] = new CurlFile($filename,'application/vnd.oasis.opendocument.text',$filebasename);
	$args['templateValues'] = $fieldValueXml;
	
	curl_setopt($ch, CURLOPT_STDERR, $fp);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $args);
	 
	$result = curl_exec($ch);    
	fwrite($fp, "Generation result: " . $result . PHP_EOL . PHP_EOL) ;  
	curl_close($ch);

	fclose($fp);
	
	return $result;

}