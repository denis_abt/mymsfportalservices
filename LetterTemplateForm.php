<!DOCTYPE html>
<HTML><HEAD>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
</HEAD>
<BODY>

<?php
date_default_timezone_set('Europe/Zurich');

$inputXml = "<?xml version=\"1.0\" encoding=\"utf-8\"?>";
$inputXml  = $inputXml .  "<foo><bar><date>".date("d.m.y")."</date><Organisation>Organisation</Organisation><FirstLast>____Denis Abt</FirstLast><Address>Elisabethenstrasse 26</Address><Zip>8004</Zip><City>Zurich</City><Country>Schweiz</Country><Year>2016</Year><Amount>1255</Amount></bar></foo>";

echo "Test with c:/Users/DAT/Documents/My Web Sites/mymsf-portal/MSF-DonationConf-D.odt";
?>

<div class="content">
<!-- Use target test.php for form post --- use LetterTemplateForm.php for CURL post -->
        <form enctype="multipart/form-data" action="PdfGeneratorService.php" method="post" accept-charset="utf-8">
            TemplateValueXml: <textarea name="templateValues" type="text" size="25"  rows="5"/><?php echo $inputXml; ?></textarea>
			<br/>
            <input type="hidden" name="MAX_FILE_SIZE" value="41942999" />
			<br/>
			<!-- Name of input element determines name in $_FILES array -->
			Send this file: <input name="userfile" type="file" />
			<br/>
            <input name="mySubmit" type="submit" value="submit" />
        </form>
 </div>

<?php

if ( isset($_FILES['userfile']) ) {
	// We require templateValues and userfile
	$filename  = $_FILES['userfile']['tmp_name'];

	$handle    = fopen($filename, "r");
	$data      = fread($handle, filesize($filename));
	/*echo "___" . $_POST['templateValues'] . "___";
	exit;*/
	$POST_DATA = array(
		'file' => base64_encode($data),
		'templateValues' =>  ""
		);
	
	$curl = curl_init();
	curl_setopt($curl, CURLOPT_URL, 'http://localhost:8080/ewsites/mymsf-portal/PdfGeneratorService.php');
	curl_setopt($curl, CURLOPT_TIMEOUT, 30);
	curl_setopt($curl, CURLOPT_POST, 1);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($curl, CURLOPT_POSTFIELDS, $POST_DATA);
	curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: text/html;charset=utf-8'));
	$response = curl_exec($curl);
	curl_close ($curl);
	echo "<h2>File Uploaded. Response: $response</h2>";
}
?>
</BODY>
</html>