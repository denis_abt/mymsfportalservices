<?php
error_reporting(E_ALL);
require __DIR__ . "/vendor/autoload.php";
require_once(__DIR__ . '/classes/Helpers.php');

$generateFromPost = false;
$verbose=false;

if (strpos($_SERVER["HTTP_HOST"], 'getunik.') !== false) {
	$data = spyc_load_file('config/config_prod.yaml');
} else {
	$data = spyc_load_file('config/config_dev.yaml');
}

$uploaddir = $data["UploadDir"];
$soffice_exe = $data["LibreOfficePathFilename"];
$target_folder = $data["WebRoot"];

// Live-Server Config
// $uploaddir = "/tmp/";	
// $soffice_exe = "/usr/bin/soffice";
// $target_folder = "/home/www-data/www/lettershop.getunik.net/";

// Handle form post parameter submit
if ((isset($_FILES['userfile']) ) && ($_FILES['userfile']['name'] != "")) {
	$uploadfile = $uploaddir . basename($_FILES['userfile']['name']);
	if (move_uploaded_file($_FILES['userfile']['tmp_name'], $uploadfile)) {
		$inputXml = $_POST['templateValues'];		
		$docStylesheet = $uploadfile;
		$generateFromPost = true;
	}
} 

date_default_timezone_set('Europe/Zurich');

require __DIR__ . "/vendor/autoload.php";
require_once(__DIR__ . '/odf-xslt/odf-xslt.php');
require_once(__DIR__ . '/classes/RootNode.php');
require_once(__DIR__ . '/classes/TemplateNode.php');
require_once(__DIR__ . '/classes/PdfGeneratorServiceClass.php');

$letterShop = new PdfGeneratorServiceClass();
$letterShop->Init($soffice_exe, $uploaddir, $target_folder, "CurlPdf_" . microtime(true));
$result_pdf = $letterShop->RenderPdfCore($inputXml, $docStylesheet);

if ($verbose) {
	echo "<!DOCTYPE html><HTML><HEAD><meta http-equiv=\"Content-Type\" content=\"text/html;charset=utf-8\"></HEAD><BODY>";
	echo "<BR>Template document: $docStylesheet" . "<BR>";
	echo "<a href='" . $result_pdf . "'>PDF</A>";
	echo "</BODY></HTML>";
} else {
	echo $result_pdf;
}
