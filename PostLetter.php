<?php
require_once './classes/Pingen.php';
require_once './classes/PingenHelper.php';

$sfile = "C:\\Users\\DAT\\Documents\\My Web Sites\\mymsf-portal\\CurlPdf_1456836966.1431.pdf";
$apiKey = "9ddb5e971dd2bcd96be3fc5f72cfbf55";
$doSendImmediatly = 1;

$pingenHelper = new PingenHelper($apiKey, 0);
$response = $pingenHelper->SubmitPdf($sfile, 1);

$causedError = $response->error;
if ($causedError) {
	echo "An error occured: " . $response->error;
} else {
	echo "Letter submitted. Id: " . $response->id;	
	// check requirement_failure, sent, rightaddress
	
	if($response->item->requirement_failure > 0)
		echo "Warning: Letter cannot be posted automatically.";
	
	// See https://www.pingen.com/en/developer/objects-document.html for details
	if ($response->item->rightaddress <> 1) 
		echo "Warning: Ensure your recipient address is aligned correctly (envelope window).";
	
}

$stats = $pingenHelper->GetStatus( $response->id);
echo "Status: " . $stats->error;

