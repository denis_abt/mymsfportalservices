<?php

/**
 * class Helpers
 *
 * Description for class Helpers
 *
 * @author:
*/
class Helpers  {

	/**
	 * Helpers constructor
	 *
	 * @param 
	 */
	function Helpers() {

	}
	
	// Use $_SERVER["HTTP_HOST"] as parameter
	static function loadConfig($httpHost){
		$data = array();
		
		if (strpos($httpHost, 'getunik.') !== false) {
			$data = spyc_load_file('config/config_prod.yaml');
		} else {
			$data = spyc_load_file('config/config_dev.yaml');
		}
		return $data;
	}
	
	static function serverOS()
	{
		$sys = strtoupper(PHP_OS);
		
		if(substr($sys,0,3) == "WIN")
		{
			$os = 1;
		}
		elseif($sys == "LINUX")
		{
			$os = 2;
		}
		else
		{
			$os = 3;
		}
		
		return $os;
	}
}

?>