<?php

/**
 * class KeyValuePdfGeneratorClass
 *
 * Description for class KeyValuePdfGeneratorClass
 *
 * @author: Denis Abt / getunik AG
*/
// This needs some overhaul, not for use at the moment.
class KeyValueLetterPdfGeneratorClass  extends PdfGeneratorClass {
	// Use this with a hierarchically correct array for xml generation
	public function RenderPdf($rootNodeName, $keysValues, $docStylesheet) {
		$inputXml = GetTemplateFieldStructure($rootNodeName, $keysValues);		
		return RenderPdfCore($inputXml, $docStylesheet);
	}
	
	// Not really adapted yet. Use the core method.
	protected function GetTemplateFieldStructure($rootNodeName, $keyValues) {
		$templateNodeSet = new RootNode();
		for ($i = 0; $i < count($keyValues); $i++) {
			$tn = $this->GetTemplateNode($keyValues[$i], $keyValues[$i]);
			$templateNodeSet->AddNode($tn);
		}
		
		/*$templateNodeFirstname = new TemplateNode("Vorname", "Denis");
		$templateNodeLastname = new TemplateNode("Lastname", "Abt");*/
		//$templateNodeBar = new TemplateNode("bar",  array($templateNodeFirstname , $templateNodeLastname));
		/*$templateNode = new TemplateNode($rootNodeName, $templateNodeBar);
		$templateNodeSet->AddNode( $templateNode);*/
		return $templateNodeSet->Render();
	}
	
	protected function GetTemplateNode($key, $value) {
		if (is_array ($value)) {
			return $this->GetTemplateNode($value[0], $value[1]);
		} else {
			return new TemplateNode($key, $value);
		}
	}	
}
