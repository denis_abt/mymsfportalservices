<?php

/**
 * class PdfGeneratorClass
 *
 * Description for class PdfGeneratorClass
 *
 * @author: Denis Abt / getunik AG
*/
class PdfGeneratorServiceClass {
	// This requires LibreOffice in headless mode, but no x-server
	protected $outputNameNoSuffix = "_tempdocument";
	protected $outputPdfSuffix = ".pdf";
	protected $outputOdtSuffix = ".odt";
	
	protected $soffice_exe = "";
	protected $cache_dir = "";
	protected $target_folder = "";	
	protected $purge_temp_libredoc = true;

	/*
	* @param $sofficePath file path to libre office executable (soffice.exe)
	*			On Ubuntu, this is most likely /usr/bin/soffice
	*/
	public function Init($sofficeUri, $cacheDir, $targetFolder, $pdfNameNoSuffix) {
		$this->soffice_exe = $sofficeUri;
		$this->cache_dir = $cacheDir;
		$this->target_folder = $targetFolder;
		$this->outputNameNoSuffix = $pdfNameNoSuffix;
		$this->EnsureLibreOfficeAccess();
	}
	
	public function RenderPdfCore($fieldDataXml, $odtFieldedTemplateDoc) {
		$this->EnsureLibreOfficeAccess();
		
		$logMessages = "";
		
		$generatedTempOdtPathFilename = $this->target_folder . ($this->outputNameNoSuffix . $this->outputOdtSuffix);
		$result_pdf_name = ($this->outputNameNoSuffix . $this->outputPdfSuffix);
		$generatedPdfPathFilename = $this->target_folder . $result_pdf_name;

		// Takes input doc and xml, and fills in xslt with $fieldDataXml values. Result in $generatedTempOdtPathFilename
		$this->XsltReplaceOdtFields($fieldDataXml, $odtFieldedTemplateDoc, $generatedTempOdtPathFilename);
		$output = $this->ConvertOdtToPdf($generatedTempOdtPathFilename);
		$logMessages .= $output . "\n";
		
		if (!copy($this->cache_dir . $result_pdf_name, $this->target_folder .  $result_pdf_name)) {
			$logMessages .= "failed to copy " . $this->cache_dir . $result_pdf_name . "...\n";
		}
		
		if ($this->purge_temp_libredoc) {
			unlink($generatedTempOdtPathFilename);
			$logMessages .= "Successfully purged temp odt file.\n";
		}
		
		return $result_pdf_name;
	}
	
	protected function GetCommandLine($inputFileNameAndPath) {
		$inputFileNamePath = stripslashes($inputFileNameAndPath);
		return $this->soffice_exe . ' -headless -invisible -convert-to pdf ' . "\""  . $inputFileNamePath . "\" --outdir $this->cache_dir";
	}
	
	protected function EnsureLibreOfficeAccess() {
		if (!file_exists($this->soffice_exe)){
			$logMessages .= PHP_EOL . "LibreOffice Exe $this->soffice_exe does not exist or is not accessible. Aborting.";
			exit(1);
		}
		
	}
	
	protected function XsltReplaceOdtFields($fieldDataXml, $odtFieldedTemplateDoc, $generatedTempOdtPathFilename) {
		$data = new DOMDocument();
		$data->loadXML($fieldDataXml);
		$processor = $this->GetProcessor($odtFieldedTemplateDoc);
		$processor->transform_to_file($data, $generatedTempOdtPathFilename);
	}
	
	protected function ConvertOdtToPdf($generatedTempOdtPathFilename) {
		// Make sure to prevent // in generatedTempOdtPhatFile
		//$command_line = $this->soffice_exe . ' -headless -invisible -convert-to pdf ' . "\""  . $generatedTempOdtPathFilename . "\" --outdir $this->cache_dir";
		$command_line = $this->GetCommandLine($generatedTempOdtPathFilename);
		$output = exec($command_line);
		return $output;
	}
	
	protected function GetProcessor($odtFieldedTemplateDoc) {
		$processor = new ODFXSLTProcessor();
		$processor->cache_dir = $this->cache_dir;
		$processor->import_stylesheet($odtFieldedTemplateDoc);
		return $processor;
	}
	
}

