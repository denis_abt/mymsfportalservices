<?php

/**
 * class PingenHelper
 *
 * Description for class PingenHelper
 *
 * @author:
*/
class PingenHelper  {

	protected $apiKey = "";
	// doesn't seem to be usable (no valid api key)
	// Helper to be used with official Pingen PHP class and -service: https://app.pingen.com
	protected $useStaging = 0;
	
	/**
	 * PingenHelper constructor
	 *
	 * @param 
	 */
	function __construct($apiKey, $useStaging = 0) {
       $this->apiKey = $apiKey;
		$this->useStaging = $useStaging;
	}
	
	protected function GetConnection() {
		if ($this->useStaging == 0)
			$api = new Pingen($this->apiKey, 1);
		else
			$api = new Pingen($this->apiKey, 2);
		
		return $api;
	}
	
	function SubmitPdf($pdfPathFilename, $doSendImmediatly) {
		$post = $this->GetConnection();
		
		$response = $post->document_upload($pdfPathFilename, $doSendImmediatly);
		return $response;
	
	}
	
	function GetStatus($documentId) {
		$post = $this->GetConnection();
		
		$response = $post->document_get($documentId);
		return $response;
	}
}

?>