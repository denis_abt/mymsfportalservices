<?php

/**
 * class RootNode
 *
 * Description for class RootNode
 *
 * @author:
*/
class RootNode  {
	protected $encoding = "ISO-8859-1";
	protected $nodeSet = array();

	/**
	 * RootNode constructor
	 *
	 * @param 
	 */
	public function __construct()
	{
	}
	
	public function AddNode(TemplateNode $node) { 
		array_push($this->nodeSet, $node);
	}
	
	public function Render() {
		echo "<?xml version=\"1.0\" encoding=\"" . $this->encoding . "\"?>". "\n";
		foreach ($this->nodeSet as $node) {
			$this->RenderNode($node);
		}
	}
	
	public function RenderNode( $nodeToRender) {
		if (is_a($nodeToRender, 'TemplateNode')) {
			if (is_string($nodeToRender->value)) {
				//echo "value is a string";
				echo "<" .  $nodeToRender->name . ">". $nodeToRender->value . "</" .  $nodeToRender->name . ">" . "\n";	
			} else {
				//echo "value is not a string";
				echo "<" .  $nodeToRender->name . ">" . "\n";	
				$this->RenderNode($nodeToRender->value);
				echo  "</" .  $nodeToRender->name. ">". "\n";
			}
		} else {
			if (is_array($nodeToRender)) {
				foreach ($nodeToRender as $node) 
					$this->RenderNode($node);
			/*} else {
				echo  $nodeToRender . "\n". "\n";	*/
			}
		}
	}
}



?>