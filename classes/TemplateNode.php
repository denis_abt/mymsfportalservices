<?php

/**
 * class TemplateNode
 *
 * Description for class TemplateNode
 *
 * @author: Denis Abt / getunik
*/
class TemplateNode  {

	/**
	 * TemplateNode constructor
	 *
	 * @param 
	 */
	public $name = null;
	public $value = null;
	
	public function __construct($nodeName, $nodeValue)
	{
		$this->name = $nodeName;
		$this->value = $nodeValue;
	}
}

?>