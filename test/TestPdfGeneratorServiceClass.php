<?php
require_once(__DIR__ . '/../vendor/autoload.php');
require_once(__DIR__ . '/../classes/RootNode.php');
require_once(__DIR__ . '/../classes/TemplateNode.php');
require_once(__DIR__ . '/../classes/PdfGeneratorServiceClass.php');

class PdfGeneratorClassTestee extends PdfGeneratorClass {
	public function GetTemplateFieldStructureP() {
		$firstname = array("Vorname", "myfourname");
		$lastname = array("Lastname", "myfinalname");
		$bar = array("bar", array($firstname, $lastname));
		return $this->GetTemplateFieldStructure("foo", $bar, $bar);
	}
}

class TestPdfGeneratorClass {
	public function setUp()
	{
		if (isset($_POST['foo'])) {
			unset($_POST['foo']);
		}
	}

	public function testGetTemplateFieldStructure()
	{
		$keys = array("blakey", "blubkey");
		$testee = new PdfGeneratorClassTestee();
		$structure = $testee->GetTemplateFieldStructureP();
		return $structure;
	}
}

$tester = new TestPdfGeneratorClass();
$xml= $tester->testGetTemplateFieldStructure();
//Assert Xml XXX